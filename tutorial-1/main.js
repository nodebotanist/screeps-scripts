const harvester = require('role.harvester')

module.exports.loop = function () {
  const spawn = Game.spawns['Spawn1']

  for( const name in Game.creeps ) {
    harvester.run(Game.creeps[name], spawn)
  }
}