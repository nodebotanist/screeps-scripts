/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('role.harvester');
 * mod.thing == 'a thing'; // true
 */

const harvester = {
  run: (creep, spawn) => {
    if( creep.carry.energy < creep.carryCapacity ) {
      const sources = creep.room.find(FIND_SOURCES)
      if( creep.harvest(sources[0]) === ERR_NOT_IN_RANGE ) {
        creep.moveTo(sources[0])
      }
    } else if ( creep.transfer(spawn, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE ) {
      creep.moveTo(spawn)
    }
  }
}

module.exports = harvester