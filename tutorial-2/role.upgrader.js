const upgrader = {
  run: (creep) => {
    const controller = creep.room.controller
    if( creep.carry.energy < creep.carryCapacity ) {
      const sources = creep.room.find(FIND_SOURCES)
      if( creep.harvest(sources[0]) === ERR_NOT_IN_RANGE ) {
        creep.moveTo(sources[0])
      }
    } else if ( creep.upgradeController(controller) === ERR_NOT_IN_RANGE ) {
      creep.moveTo(controller)
    }
  }
}

module.exports = upgrader