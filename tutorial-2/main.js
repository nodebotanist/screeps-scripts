const harvester = require('role.harvester')
const upgrader = require('role.upgrader')

module.exports.loop = function () {
  const spawn = Game.spawns['Spawn1']

  for ( let creepName in Game.creeps ) {
    let creep = Game.creeps[creepName]
    if ( creep.memory.role == 'harvester' ) {
      harvester.run(creep, spawn)
    } else if ( creep.memory.role == 'upgrader' ) {
      upgrader.run(creep)
    }
  }
}